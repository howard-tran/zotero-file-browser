# Zotero file browser

It is annoying for us to can't search collection names inside zotero. Which is super necessary to maintain a personal library. 

This small application will solve the issue by capturing zotero collection structure into a simple text file.

# Steps by steps
1. Export zotero without Files
   - <img src="Resource/img/image.png"  width="300">
   - This export will be super fast don't worry, its just metadata. The result will be a single rdf file.
2. Save as `zotero_01.rdf`
3. Run the `main.py`
4. Result is `out.txt`

```txt
001 Resource
	PRT-N1 Tools
	PRT-N2 Cool sources
002 Areas
	000 Productivity
		FORG000 File organization
		FORG001 File org, Library
		LHWTL000 Learn how to learn
		PRD000 Productivity general
		RDNG000 Reading
		ZET000 Zettelkasten
			To Read
			To Support
	001 Health
		CFEE00 Coffee
		MOTINT00 Motivated midnight
		SGUCA00 Sugar & carb
		SKCR00MT Skincare, mụn thâm
		TENTAVAC0 tetatnus vac
```

# Make the script utilities

- Requirement
  - Python3 in your classpath

```bash
mv ./main.py ~/bin/zotero_col.py

# Edit your ~/.bashrc
# we need a space
alias zot_file="python3 ~/bin/zotero_col.py "
```

Usages

```bash
zot_file "your_library.rdf" "output_metadata.txt"
```
