from bs4 import BeautifulSoup as bs
import bs4
import os
import sys

zotero_rdf = sys.argv[1]
out_file = "out.txt"
if len(sys.argv) > 2:
    out_file = sys.argv[2]
xml = open(zotero_rdf, encoding="utf-8")
soup = bs(xml, "lxml-xml")

node = soup.find("z:Collection")
def traverse(node: bs4.element.Tag):
    arr_childs = {}
    dict_info = {}

    while (node != None):
        node_id = node.get("rdf:about")
        if node_id != None:
            node_name = node.find("dc:title").text
            dict_info[node_id] = {"name": node_name}
            arr_childs[node_id] = {}
            for x in node.findChildren():
                if x.get("rdf:resource") != None and "#collection" in x.get("rdf:resource"):
                    arr_childs[node_id][x.get("rdf:resource")] = {}

        node = node.findNextSibling()

    return [arr_childs, dict_info]

[childs_arr, childs_info] = [*traverse(node)]

def print_structure(childs_arr, childs_arr_2, childs_info, childs_flag, file, str_pad = ""):
    for child_key in childs_arr:
        if childs_flag[child_key]:
            continue
        file.write(f'\n{str_pad}{childs_info[child_key]["name"]}')
        if len(childs_arr_2[child_key]) > 0:
            print_structure(childs_arr_2[child_key], childs_arr_2, childs_info,
                            childs_flag, file, "    " + str_pad)
        childs_flag[child_key] = True

childs_flag = {}
for child in childs_info:
    childs_flag[child] = False

file = open(out_file, "w+", encoding="utf-8")
print_structure(childs_arr, childs_arr, childs_info, childs_flag, file)
